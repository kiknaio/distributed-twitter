const level = require('level');
const hyperlog = require('hyperlog');
const to = require('to2');
const sha256 = require('crypto-js/sha256');
const { parseArguments } = require('./helpers');
const db = level('logs.db');
const feed = hyperlog(db, { valueEncoding: 'json' });

const actions = {
  get: hash => 
		feed.createReadStream()
			.pipe(to.obj((row, enc, next) => {
				if(hash === row.key) {
					console.log(row);
				}
				next();
			})),
  set: args =>
    feed.add(null, { content: args.join(' ') }, err =>
      console.log(err ? err : 'Data saved')
    ),
	feed: () =>
		feed.heads((err, posts) => {
			if(err) return console.error(err);
			console.log(posts);
		})
};

const method = process.argv[2];

if (method in actions) {
  actions[method](parseArguments(process.argv))
} else {
	console.log('Provided method is unknown');
}
